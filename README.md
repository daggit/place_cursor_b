# place_cursor_b

BASICv2 method of positioning a cursor to screen location X, Y.  Uses Kernel PLOT code to actually place cursor.